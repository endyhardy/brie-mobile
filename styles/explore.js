import { StyleSheet } from 'react-native';
import * as constants from "./values";

export default StyleSheet.create({

  categoriesListItemLeft: {
    width:              '50%',
    paddingRight:       constants.MARGIN.BASE / 4,
    paddingBottom:      constants.MARGIN.BASE / 2,
  },

  categoriesListItemRight: {
    width:              '50%',
    paddingLeft:        constants.MARGIN.BASE / 4,
    paddingBottom:      constants.MARGIN.BASE / 2,
  },

  categoriesListItemTouchable: {
    borderRadius:       6,
    backgroundColor:    constants.COLORS.WHITE,
    paddingVertical:    constants.MARGIN.BASE,
    paddingHorizontal:  constants.MARGIN.BASE,
  },

  categoriesListItemText: {
    color:      constants.COLORS.GRAY.DARKEST,
    fontSize:   constants.FONT_SIZE.HEADING6,
    textAlign:  'center',
    fontFamily: constants.FONT_FAMILY_SANS_SERIF,
  },

});