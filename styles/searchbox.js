import { StyleSheet } from 'react-native';
import * as constants from "./values";

export default StyleSheet.create({

  upperToolbarContainer: {
    borderTopColor:     constants.COLORS.GRAY.LIGHT,
    borderTopWidth:     1,
    backgroundColor:    constants.COLORS.WHITE,
    paddingVertical:    constants.SPACING.X3,
    paddingHorizontal:  constants.MARGIN.BASE,
  },

  inputContainer: {
    position: 'relative'
  },

  input: {
    borderRadius:       22,
    backgroundColor:    constants.COLORS.GRAY.LIGHTEST,
    paddingVertical:    constants.SPACING.X3,
    paddingHorizontal:  constants.SPACING.X3,
    fontFamily:         constants.FONT_FAMILY_SANS_SERIF,
    fontSize:           constants.FONT_SIZE.BASE,
  },

  inputIcon: {
    top:      9,
    right:    12,
    width:    24,
    height:   24,
    position: 'absolute',
  }

});