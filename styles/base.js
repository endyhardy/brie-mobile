import { StyleSheet } from 'react-native';
import * as constants from "./values";

export default StyleSheet.create({

  text: {
    color:      constants.COLORS.GRAY.DARKEST,
    fontSize:   constants.FONT_SIZE.BASE,
    fontFamily: constants.FONT_FAMILY_SANS_SERIF,
  },

  textMuted: {
    color:      constants.COLORS.GRAY.NORMAL,
    fontSize:   constants.FONT_SIZE.BASE,
    fontFamily: constants.FONT_FAMILY_SANS_SERIF,
  },

  textPrimary: {
    color:      constants.COLOR_PRIMARY,
    fontSize:   constants.FONT_SIZE.BASE,
    fontFamily: constants.FONT_FAMILY_SANS_SERIF,
  },

  textStrong: {
    color:      constants.COLORS.GRAY.DARKEST,
    fontSize:   constants.FONT_SIZE.BASE,
    fontFamily: constants.FONT_FAMILY_SANS_SERIF,
    fontWeight: constants.FONT_WEIGHT_BOLD,
  },

  textSectionHeadingTitle: {
    color:      constants.COLORS.GRAY.DARKEST,
    fontSize:   constants.FONT_SIZE.HEADING3,
    fontFamily: constants.FONT_FAMILY_HEADINGS,
  },

  contentContainer: {
    paddingHorizontal: constants.MARGIN.BASE,
  },
  section: {
    paddingTop:     constants.MARGIN.X2,
    paddingBottom:  constants.MARGIN.BASE,
  },
  sectionHeading: {
    marginBottom: constants.MARGIN.BASE,
  },
  upperToolbarContainer: {
    borderTopColor:     constants.COLORS.GRAY.LIGHT,
    borderTopWidth:     1,
    backgroundColor:    constants.COLORS.WHITE,
    paddingVertical:    constants.SPACING.X3,
    paddingHorizontal:  constants.MARGIN.BASE,
  },

  spacer: {
    height: constants.MARGIN.BASE
  },

})