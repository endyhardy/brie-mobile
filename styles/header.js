import * as constants from "./values";

export default {
  headerStyle: {
    height:           60,
    backgroundColor:  constants.COLORS.WHITE,
  },
  headerTintColor: constants.COLOR_PRIMARY,
  headerTitleStyle: {
    color:      constants.COLORS.GRAY.DARKEST,
    fontSize:   constants.FONT_SIZE.HEADING5,
    marginTop:  5,
    fontFamily: constants.FONT_FAMILY_HEADINGS,
  }
};