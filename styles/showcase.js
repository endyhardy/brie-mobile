import { StyleSheet } from 'react-native';
import * as constants from './values';

export default StyleSheet.create({

  container: {
    backgroundColor: constants.COLORS.WHITE,
  },

  header: {
    flexDirection: 'row',
    marginTop: constants.MARGIN.BASE,
    marginBottom: constants.MARGIN.BASE,
  },

  headerText: {
    flex: 1,
  },

  avatar: {
    width: 50,
    height: 50,
    borderRadius: 25,
    marginRight: constants.MARGIN.BASE
  },

  message: {
    fontSize: constants.FONT_SIZE.HEADING5,
    fontFamily: constants.FONT_FAMILY_SANS_SERIF,
    marginBottom: constants.MARGIN.BASE,
  },

  image: {
    marginBottom: constants.MARGIN.BASE,
  }

});