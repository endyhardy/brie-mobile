import { Dimensions, Platform, StyleSheet } from 'react-native';
import * as constants from "./values";

export default StyleSheet.create({

  container: {
    height:           Platform.OS === 'ios' && (Dimensions.get('window').height === 812 || Dimensions.get('window').width === 812) ? 80 : 60,
    zIndex:           constants.ZINDEX_TOOLBAR,
    flexDirection:    'row',
    alignItems:       'flex-start',
    borderTopColor:   constants.COLORS.GRAY.LIGHT,
    borderTopWidth:   1,
    backgroundColor:  constants.COLORS.WHITE,
  },

  touchable: {
    width:          '25%',
    height:         60,
    alignItems:     'center',
    justifyContent: 'center',
  }

})