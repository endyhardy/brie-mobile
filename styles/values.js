// Colors

export const COLORS = {
  BLACK: 'black',
  WHITE: 'white',
  GRAY: {
    LIGHTEST: '#eee',
    LIGHT:    '#ccc',
    NORMAL:   '#999',
    DARK:     '#666',
    DARKEST:  '#333',
  },

  GREEN:  '#159957',
  BLUE:   '#155799',
};
export const COLOR_PRIMARY = COLORS.GREEN;
export const COLOR_SECONDARY = COLORS.BLUE;

// Fonts

export const FONT_FAMILY_SANS_SERIF = 'Oxygen';
export const FONT_FAMILY_HEADINGS = 'Josefin Sans';
export const FONT_WEIGHT_NORMAL = 'normal';
export const FONT_WEIGHT_BOLD = 'bold';
export const FONT_SIZE = {
  BASE: 16,
  HEADING6: 16,
  HEADING5: 20,
  HEADING4: 24,
  HEADING3: 28,
  HEADING2: 32,
  HEADING1: 36,
};

// Spacing and Margins

export const SPACING = {
  BASE: 4,
  X2:   8,
  X3:   12,
  X4:   16,
  X5:   24,
};
export const MARGIN = {
  BASE: SPACING.BASE * 4,
  X2:   SPACING.BASE * 6
};

// Z-Index

export const ZINDEX_TOOLBAR = 1000;