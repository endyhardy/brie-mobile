import { Dimensions, StyleSheet } from 'react-native';
import * as constants from "./values";

export default StyleSheet.create({

  feedItem: {
    flexDirection:      'row',
    backgroundColor:    constants.COLORS.WHITE,
    paddingVertical:    constants.MARGIN.BASE,
    paddingHorizontal:  constants.MARGIN.BASE,
    borderBottomColor:  constants.COLORS.GRAY.LIGHTEST,
    borderBottomWidth:  1,
  },

  feedItemAvatarContainer: {
    width: 64,
  },

  feedItemAvatar: {
    width:        50,
    height:       50,
    borderRadius: 25,
  },

  feedItemContentContainer: {
    flex: 1,
  },

  feedItemTextContainer: {
    marginBottom: constants.SPACING.X3,
  },

  feedItemTextHeadingContainer: {
    marginBottom:   constants.SPACING.BASE,
    flexDirection:  'row',
    justifyContent: 'space-between',
  },

  feedItemUserName: {
    color:      constants.COLORS.GRAY.DARKEST,
    fontSize:   constants.FONT_SIZE.BASE,
    fontFamily: constants.FONT_FAMILY_SANS_SERIF,
    fontWeight: constants.FONT_WEIGHT_BOLD,
  },

  feedItemImage: {
    width:        '100%',
    height:       Dimensions.get('window').width / 2.2,
    borderColor:  constants.COLORS.GRAY.LIGHTEST,
    borderWidth:  1,
    borderRadius: 10,
    marginBottom: constants.MARGIN.BASE,
  },

  feedItemActionsContainer: {
    flexDirection:  'row',
    justifyContent: 'space-between'
  },
  feedItemActionsLeft: {
    flexDirection: 'row'
  },
  feedItemActionsLeftItem: {
    alignItems:     'center',
    marginRight:    constants.SPACING.X3,
    flexDirection:  'row',
  },
  feedItemActionsLeftItemIcon: {
    width:        22,
    height:       22,
    marginRight:  constants.SPACING.BASE
  },

});