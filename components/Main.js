import React, { Component, Fragment } from 'react';
import { View } from 'react-native';

import Toolbar from './Toolbar';
import Feed from '../pages/Feed';
import Explore from '../pages/Explore';

class Main extends Component {
  state = {
    ready: false,
    section: 'feed',
  }

  navigate(section) {
    this.setState({ section });
  }

  render() {
    return (
      <View style={{ justifyContent: 'space-between', height: '100%' }}>
        <View style={{ flex: 1 }}>
          { this.state.section === 'feed' && <Feed /> }
          { this.state.section === 'explore' && <Explore /> }
        </View>

        <Toolbar active={ this.state.section } onNavigate={ section => this.navigate(section) } />
      </View>
    );
  }
}

export default Main;