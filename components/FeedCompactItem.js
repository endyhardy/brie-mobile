import React from 'react';
import { Dimensions, Image, Text, View } from 'react-native';
import Touchable from "react-native-platform-touchable";

import { Base as BaseStyles, Feed as FeedStyles } from "../styles";

const FeedCompactItem = props => (
  <Touchable key={ props.id }>
    <View style={{ width: Dimensions.get('window').width * 0.7, paddingRight: 16 }}>
      <View style={{ flexDirection: 'row', marginBottom: 12, flexWrap: 'nowrap' }}>
        <Image source={{ uri: props.user.data.avatar }} style={ FeedStyles.feedItemAvatar } />
        <View style={{ paddingLeft: 12, flex: 1 }}>
          <Text style={ FeedStyles.feedItemUserName } numberOfLines={ 1 }>{ props.user.data.name }</Text>
          <Text style={ BaseStyles.text } numberOfLines={ 1 }>{ props.message }</Text>
        </View>
      </View>
      <Image source={{ uri: props.images[0].uri }} style={{ height: Dimensions.get('window').width / 2.4, width: '100%', borderRadius: 10, borderColor: '#eee', borderWidth: 1, marginBottom: 12 }} />
      <View style={ FeedStyles.feedItemActionsContainer }>
        <View style={ FeedStyles.feedItemActionsLeft }>
          <Touchable>
            <View style={ FeedStyles.feedItemActionsLeftItem }>
              {
                props.liked ?
                  <Image source={ require('../resources/images/LikeActive.png') } style={ FeedStyles.feedItemActionsLeftItemIcon } /> :
                  <Image source={ require('../resources/images/LikeIdle.png') } style={ FeedStyles.feedItemActionsLeftItemIcon } />
              }
              <Text style={ props.liked ? BaseStyles.textPrimary : BaseStyles.textMuted }>
                { props.likes_count }
              </Text>
            </View>
          </Touchable>

          <Touchable>
            <View style={ FeedStyles.feedItemActionsLeftItem }>
              <Image source={ require('../resources/images/Comment.png') } style={ FeedStyles.feedItemActionsLeftItemIcon } />
              <Text style={ BaseStyles.textMuted }>
                { props.comments_count }
              </Text>
            </View>
          </Touchable>
        </View>
      </View>
    </View>
  </Touchable>
);

export default FeedCompactItem;