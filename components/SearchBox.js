import React, { Component } from 'react';
import { Image, Text, TextInput, View } from 'react-native';

import { Searchbox as Styles } from "../styles";

class SearchBox extends Component {
  state = {  }
  render() {
    return (
      <View style={ Styles.upperToolbarContainer }>
        <View style={ Styles.inputContainer }>
          <TextInput
            style={ Styles.input }
            underlineColorAndroid="rgba(0,0,0,0)"
            placeholderTextColor="#999"
            { ...this.props.inputProps } />

          <Image source={ require('../resources/images/ExploreIdle.png') } style={ Styles.inputIcon } />
        </View>
      </View>
    );
  }
}

export default SearchBox;