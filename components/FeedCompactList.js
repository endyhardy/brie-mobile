import React from 'react';
import { ActivityIndicator, Dimensions, Image, ScrollView, Text, View } from 'react-native';
import Touchable from "react-native-platform-touchable";

import { Base as BaseStyles, Feed as FeedStyles, base } from "../styles";
import FeedCompactItem from './FeedCompactItem';

const FeedCompactList = props => (
  <View style={ BaseStyles.section }>
    <View style={ BaseStyles.contentContainer }>
      <Text style={ BaseStyles.textSectionHeadingTitle }>{ props.title }</Text>
      { props.subtitle && <Text style={ BaseStyles.textMuted }>{ props.subtitle }</Text> }
    </View>

    <View style={ BaseStyles.spacer } />

    { props.fetching && (!props.data.length || !props.data) && <ActivityIndicator /> }
    <ScrollView horizontal={ true } showsHorizontalScrollIndicator={ false } style={{ paddingLeft: 16 }}>
      {
        props.data &&
        props.data.map(feedItem => <FeedCompactItem key={ feedItem.id } { ...feedItem } />)
      }
      <View style={{ width: 16 }} />
    </ScrollView>

    <View style={ BaseStyles.spacer } />

    <View style={ BaseStyles.contentContainer }>
      <Touchable hitSlop={{ top: 5, bottom: 5, left: 5, right: 5 }}>
        <Text style={ BaseStyles.textPrimary }>More updates</Text>
      </Touchable>
    </View>
  </View>
);

export default FeedCompactList;