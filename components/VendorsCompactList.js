import React from 'react';
import { ActivityIndicator, Image, Text, View } from 'react-native';
import Touchable from "react-native-platform-touchable";
import Lo from "lodash";

import { Base as BaseStyles, Feed as FeedStyles, base } from "../styles";

const VendorsCompactList = props => (
  <View style={ BaseStyles.section }>
    <View style={ BaseStyles.contentContainer }>
      <Text style={ BaseStyles.textSectionHeadingTitle }>{ props.title }</Text>
      { props.subtitle && <Text style={ BaseStyles.textMuted }>{ props.subtitle }</Text> }
    </View>
    <View style={ BaseStyles.spacer } />

    { props.fetching && <ActivityIndicator /> }
    {
      props.data &&
      props.data.map(vendor => (
        <View key={ vendor.user.data.id } style={ FeedStyles.feedItem }>
          <View style={ FeedStyles.feedItemAvatarContainer }>
            <Image source={{ uri: vendor.user.data.avatar }} style={ FeedStyles.feedItemAvatar } />
          </View>
          <View style={ FeedStyles.feedItemContentContainer }>
            <View style={ FeedStyles.feedItemTextHeadingContainer }>
              <Text style={ FeedStyles.feedItemUserName }>{ vendor.user.data.name }</Text>
            </View>
            <Text style={ BaseStyles.textMuted }>
              Makassar, ID -
              {
                vendor.reviews.count === 0 ?
                ' No reviews yet' :
                ' Score ' + Lo.round(vendor.reviews.score, 1) + ' (' +  vendor.reviews.count + ')'
              }</Text>
          </View>
        </View>
      ))
    }

    <View style={ BaseStyles.spacer } />

    <View style={ BaseStyles.contentContainer }>
      <Touchable hitSlop={{ top: 5, bottom: 5, left: 5, right: 5 }}>
        <Text style={ BaseStyles.textPrimary }>See all vendors</Text>
      </Touchable>
    </View>
  </View>
);

export default VendorsCompactList;