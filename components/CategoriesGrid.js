import React from 'react';
import { ActivityIndicator, Text, View } from 'react-native';
import { View as AnimView } from 'react-native-animatable';
import Touchable from "react-native-platform-touchable";

import { Base as BaseStyles, Explore as Styles } from "../styles";

const CategoriesGrid = props => (
  <View style={ BaseStyles.section }>
    <View style={ BaseStyles.contentContainer }>
      <View style={ BaseStyles.sectionHeading }>
        <Text style={ BaseStyles.textSectionHeadingTitle }>Browse Categories</Text>
        <Text style={ BaseStyles.textMuted }>What do you need?</Text>
      </View>
    </View>
    { props.fetching && props.data.length === 0 && <ActivityIndicator /> }
    {
      props.data.length > 0 && (
        <View style={ BaseStyles.contentContainer }>
          <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
            {
              props.data.map((category, index) => (
                <AnimView
                  key={ category.id }
                  animation={ index % 2 === 0 ? "fadeInLeft" : "fadeInRight" }
                  duration={ 250 }
                  delay={ index * 50 }
                  easing="ease-out"
                  style={ index % 2 === 0 ? Styles.categoriesListItemLeft : Styles.categoriesListItemRight }>
                  <Touchable onPress={ () => props.onNavigate({ category }) } style={ Styles.categoriesListItemTouchable }>
                    <Text numberOfLines={ 1 } style={ Styles.categoriesListItemText }>{ category.name }</Text>
                  </Touchable>
                </AnimView>
              ))
            }
          </View>
        </View>
      )
    }
  </View>
);

export default CategoriesGrid;