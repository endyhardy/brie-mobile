import React from 'react';
import { Dimensions, Image, Text, View } from 'react-native';
import Touchable from "react-native-platform-touchable";

import { Base as BaseStyles, Feed as Styles } from "../styles";

const FeedItem = props => (
  <View style={ Styles.feedItem }>
    <View style={ Styles.feedItemAvatarContainer }>
      <Image source={{ uri: props.item.user.data.avatar }} style={ Styles.feedItemAvatar } />
    </View>

    <View style={ Styles.feedItemContentContainer }>
      <View style={ Styles.feedItemTextContainer }>
        <View style={ Styles.feedItemTextHeadingContainer }>
          <Text style={ Styles.feedItemUserName } numberOfLines={ 1 }>
            { props.item.user.data.name }
          </Text>
          <Text style={ BaseStyles.textMuted }>
            { props.item.category + ' - ' + props.item.created_at }
          </Text>
        </View>
        {
          props.item.message.length > 0 &&
          (
            <Text style={ BaseStyles.text }>
              { props.item.message }
            </Text>
          )
        }
      </View>

      {
        props.item.images.length > 0 &&
        (
          <Touchable onPress={ () => props.onOpenDetails() }>
            <Image source={{ uri: props.item.images[0].uri }} style={ Styles.feedItemImage } />
          </Touchable>
        )
      }

      <View style={ Styles.feedItemActionsContainer }>
        <View style={ Styles.feedItemActionsLeft }>
          <Touchable>
            <View style={ Styles.feedItemActionsLeftItem }>
              {
                props.item.liked ?
                  <Image source={ require('../resources/images/LikeActive.png') } style={ Styles.feedItemActionsLeftItemIcon } /> :
                  <Image source={ require('../resources/images/LikeIdle.png') } style={ Styles.feedItemActionsLeftItemIcon } />
              }
              <Text style={ props.item.liked ? BaseStyles.textPrimary : BaseStyles.textMuted }>
                { props.item.likes_count }
              </Text>
            </View>
          </Touchable>

          <Touchable>
            <View style={ Styles.feedItemActionsLeftItem }>
              <Image source={ require('../resources/images/Comment.png') } style={ Styles.feedItemActionsLeftItemIcon } />
              <Text style={ BaseStyles.textMuted }>
                { props.item.comments_count }
              </Text>
            </View>
          </Touchable>
        </View>
      </View>
    </View>
  </View>
);

export default FeedItem;