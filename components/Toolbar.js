import React from 'react';
import { Image, View } from 'react-native';
import Touchable from "react-native-platform-touchable";

import { Toolbar as Style } from "../styles";

const Toolbar = props => (
  <View style={ Style.container }>
    <Touchable style={ Style.touchable } onPress={ () => props.onNavigate('feed') }>
      {
        props.active === 'feed' ?
          <Image source={ require('../resources/images/FeedActive.png') } height="28" width="28" /> :
          <Image source={ require('../resources/images/FeedIdle.png') } height="28" width="28" />
      }
    </Touchable>
    <Touchable style={ Style.touchable } onPress={ () => props.onNavigate('explore') }>
      {
        props.active === 'explore' ?
          <Image source={ require('../resources/images/ExploreActive.png') } height="28" width="28" /> :
          <Image source={ require('../resources/images/ExploreIdle.png') } height="28" width="28" />
      }
    </Touchable>
    <Touchable style={ Style.touchable } onPress={ () => props.onNavigate('notifications') }>
      {
        props.active === 'notifications' ?
          <Image source={ require('../resources/images/NotificationsActive.png') } height="28" width="28" /> :
          <Image source={ require('../resources/images/NotificationsIdle.png') } height="28" width="28" />
      }
    </Touchable>
    <Touchable style={ Style.touchable } onPress={ () => props.onNavigate('my') }>
      {
        props.active === 'my' ?
          <Image source={ require('../resources/images/MyActive.png') } height="28" width="28" /> :
          <Image source={ require('../resources/images/MyIdle.png') } height="28" width="28" />
      }
    </Touchable>
  </View>
);

export default Toolbar;