import React, { Component } from 'react';
import { Image, Text, View } from 'react-native';
import Touchable from "react-native-platform-touchable";

import { Base as BaseStyles } from "../styles";

class SingleShowcaseActions extends Component {
  state = {  }
  render() {
    return (
      <View style={ BaseStyles.upperToolbarContainer }>
        <View style={{ flexDirection: 'row', alignItems: 'center', marginBottom: 12 }}>
          <View style={{ flexDirection: 'row', alignItems: 'center', marginRight: 16 }}>
          {
            this.props.liked ?
              <Image source={ require('../resources/images/LikeActive.png') } style={{ height: 20, width: 20, marginRight: 8 }} /> :
              <Image source={ require('../resources/images/LikeIdle.png') } style={{ height: 20, width: 20, marginRight: 8 }} />
          }
            <Text style={ BaseStyles.text }>{ this.props.likes_count } Likes</Text>
          </View>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Image source={ require('../resources/images/Comment.png') } style={{ height: 20, width: 20, marginRight: 8 }} />
            <Text style={ BaseStyles.text }>{ this.props.comments_count } Comments</Text>
          </View>
        </View>
        <Touchable style={{ backgroundColor: '#eee', paddingHorizontal: 12, paddingVertical: 12, borderRadius: 22 }}>
          <Text style={ BaseStyles.textMuted }>Comment on this showcase..</Text>
        </Touchable>
      </View>
    );
  }
}

export default SingleShowcaseActions;