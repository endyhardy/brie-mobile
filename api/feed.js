import Axios from "axios";
import { API_BASE_URL } from "../config/app";

export default class Api {

  static index({ nextURL = null, cb, err }) {
    return Axios.get(nextURL || API_BASE_URL + '/feed')
      .then(response =>
        cb({
          data: response.data.data,
          nextURL: response.data.meta.pagination.links.next
        })
      )
      .catch(e => err(e));
  }

}