import Axios from "axios";
import { API_BASE_URL } from "../config/app";

export default class Api {

  static index({ cb, err }) {
    return Axios.get(API_BASE_URL + '/categories')
      .then(response =>
        cb({ data: response.data.data })
      )
      .catch(e => err(e));
  }

  static show({ category, cb, err, cancelToken }) {
    return Axios.get(API_BASE_URL + '/categories/' + category.id, {
      cancelToken: new Axios.CancelToken(c => { cancelToken = c; })
    })
      .then(response => cb({ data: response.data }))
      .catch(e => err(e));
  }

}