import { FEED } from "./types";
import { Feed as FeedApi } from '../api';

export const getFeed = ({ reload = false }) =>
  (dispatch, getState) => {
    if (getState().feed.fetching)
      return;

    if (reload)
      dispatch(resetFeed());

    dispatch(invalidateFeed());

    return FeedApi.index({
      cb: (feed) => dispatch(populateFeed({ feed })),
      err: () => dispatch(setErrorFeed()),
    });
  }

export const invalidateFeed = () => {
  return { type: FEED.INVALIDATE };
}

export const populateFeed = ({ feed }) => {
  return { type: FEED.POPULATE, data: feed.data, nextURL: feed.nextURL };
}

export const resetFeed = () => {
  return { type: FEED.RESET };
}

export const setErrorFeed = () => {
  return { type: FEED.SET_ERROR };
}