import { CATEGORIES } from "./types";
import { Categories as CategoriesApi } from '../api';

export const getCategories = ({ reload }) =>
  (dispatch, getState) => {
    if (getState().categories.fetching)
      return;

    if (reload)
      dispatch(resetCategories());

    dispatch(invalidateCategories());

    return CategoriesApi.index({
      cb: (categories) => dispatch(populateCategories({ categories })),
      err: () => dispatch(setErrorCategories()),
    });
  }

export const invalidateCategories = () => {
  return { type: CATEGORIES.INVALIDATE };
}

export const populateCategories = ({ categories }) => {
  return { type: CATEGORIES.POPULATE, data: categories.data };
}

export const resetCategories = () => {
  return { type: CATEGORIES.RESET };
}

export const setErrorCategories = () => {
  return { type: CATEGORIES.SET_ERROR };
}