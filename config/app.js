import {
    ENV,
    BASE_URL as ENV_BASE_URL
} from "../env";

/**
 * Base URLs configuration
 */
export const BASE_URL = ENV_BASE_URL[ENV] || ENV_BASE_URL.PROD;
export const API_BASE_URL = BASE_URL + '/api';