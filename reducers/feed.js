import { FEED } from "../actions/types";

export default (
    state = {
      data:     [],
      error:    false,
      nextURL:  '',
      fetching: false,
    },
    action
  ) => {

    newState = Object.assign({}, state);

    switch (action.type) {

      case FEED.POPULATE:
        newState.data = action.data.slice(0);
        newState.nextURL = action.nextURL;
        newState.fetching = false;
        return newState;

      case FEED.INVALIDATE:
        newState.error = false;
        newState.fetching = true;
        return newState;

      case FEED.RESET:
        newState.data = [];
        newState.nextURL = '';
        return newState;

      case FEED.SET_ERROR:
        newState.error = true;
        newState.fetching = false;
        return newState;

      default: return state;
    }

  }