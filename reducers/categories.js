import { CATEGORIES } from "../actions/types";

export default (
    state = {
      data:     [],
      error:    false,
      fetching: false,
    },
    action
  ) => {

    newState = Object.assign({}, state);

    switch (action.type) {

      case CATEGORIES.POPULATE:
        newState.data = action.data.slice(0);
        newState.fetching = false;
        return newState;

      case CATEGORIES.INVALIDATE:
        newState.error = false;
        newState.fetching = true;
        return newState;

      case CATEGORIES.RESET:
        newState.data = [];
        return newState;

      case CATEGORIES.SET_ERROR:
        newState.error = true;
        newState.fetching = false;
        return newState;

      default: return state;
    }

  }