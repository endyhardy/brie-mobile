import { combineReducers } from 'redux';

import feed from "./feed";
import categories from "./categories";

export default combineReducers({
    feed,
    categories,
});