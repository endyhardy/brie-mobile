import React, { Component } from 'react';
import { ActivityIndicator, FlatList, Text, View } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { createStackNavigator } from "react-navigation";

import * as actions from '../actions'
import { Base as BaseStyles, Header as HeaderStyles } from "../styles";
import { FeedItem } from "../components";
import Showcase from './Showcase';

class Feed extends Component {

  state = {

  };

  static navigationOptions = {
    title: 'Feed',
  }

  componentDidMount() {
    this.props.getFeed({ reload: false });
  }

  render() {
    return (
      <View style={{ height: '100%' }}>
        {
          this.props.feed.data.length > 0 &&
          <FlatList
            keyExtractor={ item => 'feed-item-' + item.id }
            data={ this.props.feed.data }
            renderItem={ showcase => <FeedItem { ...showcase } onOpenDetails={ () => this.props.navigation.navigate('Showcase', { showcase: showcase.item }) } /> } />
        }
      </View>
    );
  }
}

const FeedWithRedux = connect(
  (state, props) => {
    return {
      feed: state.feed,
    };
  },
  dispatch => bindActionCreators(actions, dispatch)
)(Feed)

export default createStackNavigator({
  Home: FeedWithRedux,
  Showcase,
},
{
  initialRouteName: 'Home',
  navigationOptions: {
    ...HeaderStyles
  }
});