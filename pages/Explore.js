import React, { Component } from 'react';
import { ScrollView, View } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { createStackNavigator } from "react-navigation";

import * as actions from '../actions'
import { Header as HeaderStyles } from "../styles";
import CategoriesGrid from '../components/CategoriesGrid';
import SearchBox from '../components/SearchBox';
import Category from './Category';

class Explore extends Component {

  state = {

  };

  static navigationOptions = {
    title: 'Explore',
  }

  componentDidMount() {
    this.props.getCategories({ reload: false });
  }

  render() {
    return (
      <View style={{ height: '100%' }}>
        <ScrollView>
          <CategoriesGrid { ...this.props.categories } onNavigate={ ({ category }) => this.props.navigation.navigate('Category', { category }) } />
        </ScrollView>

        <SearchBox inputProps={{ placeholder: 'Look for wedding stuff..' }} />
      </View>
    );
  }
}

const ExploreWithRedux = connect(
  (state, props) => {
    return {
      categories: state.categories,
    };
  },
  dispatch => bindActionCreators(actions, dispatch)
)(Explore)

export default createStackNavigator({
  Home: ExploreWithRedux,
  Category: Category,
},
{
  initialRouteName: 'Home',
  navigationOptions: {
    ...HeaderStyles
  }
});