import React, { Component, Fragment } from 'react';
import { Dimensions, Image, ScrollView, Text, View } from 'react-native';
import Touchable from "react-native-platform-touchable";

import { Base as BaseStyles, Showcase as Styles } from "../styles";
import SingleShowcaseActions from '../components/SingleShowcaseActions';

class Showcase extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showcase: Object.assign({}, props.navigation.state.params.showcase)
    };
  }

  static navigationOptions = ({ navigation }) => ({
    title: navigation.state.params.showcase.category,
  })

  render() {
    return (
      <Fragment>
        <ScrollView style={ Styles.container }>
          <View style={ BaseStyles.contentContainer }>
            <Touchable>
              <View style={ Styles.header }>
                <Image source={{ uri: this.state.showcase.user.data.avatar }} style={ Styles.avatar } />

                <View style={ Styles.headerText }>
                  <Text style={ BaseStyles.textStrong }>{ this.state.showcase.user.data.name }</Text>
                  <Text style={ BaseStyles.textMuted }>Showcase</Text>
                </View>
              </View>
            </Touchable>

            <Text style={ Styles.message }>{ this.state.showcase.message }</Text>

          </View>
          {
            this.state.showcase.images.map((image, key) => (
              <View style={ Styles.image } key={ key }>
                <Image
                  source={{ uri: image.uri }}
                  style={{ width: '100%', height: (Dimensions.get('window').width / image.width) * image.height }} />
              </View>
            ))
          }

        </ScrollView>

        <SingleShowcaseActions { ...this.state.showcase } />
      </Fragment>
    );
  }
}

export default Showcase;