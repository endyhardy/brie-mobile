import React, { Component } from 'react';
import { ScrollView, View } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { createStackNavigator } from "react-navigation";

import * as actions from '../actions'
import { Categories as CategoriesApi } from "../api";
import VendorsCompactList from '../components/VendorsCompactList';
import FeedCompactList from '../components/FeedCompactList';

class Category extends Component {

  state = {
    data:     [],
    error:    false,
    fetching: false,
  };
  cancelToken = false;

  static navigationOptions = ({ navigation }) => ({
    title: 'Explore: ' + navigation.state.params.category.name,
  })

  componentDidMount() {
    this.load();
  }

  componentWillUnmount() {
    this.cancelToken && this.cancelToken();
  }

  load() {
    if (this.state.fetching)
      return;

    this.setState({ fetching: true, error: false });
    return CategoriesApi.show({
      category: this.props.navigation.state.params.category,
      cb: ({ data }) => this.setState({ fetching: false, data }),
      err: () => this.setState({ fetching: false, error: true }),
      cancelToken: this.cancelToken
    })
  }

  render() {
    return (
      <ScrollView style={{ height: '100%' }}>

        <FeedCompactList
          title="Recent Updates"
          subtitle="See new showcases from top vendors"
          data={ this.state.data.feed && this.state.data.feed.data } />

        <VendorsCompactList
          title="Vendors"
          subtitle={ "Top " + this.props.navigation.state.params.category.name + " vendors near you" }
          data={ this.state.data.vendors && this.state.data.vendors.data } />

      </ScrollView>
    );
  }
}

export default connect(
  (state, props) => {
    return {
      categories: state.categories,
    };
  },
  dispatch => bindActionCreators(actions, dispatch)
)(Category)