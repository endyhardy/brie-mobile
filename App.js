import React from 'react';
import { Provider } from 'react-redux';

import Store from './store'
import Main from './components/Main'

export default () => (
  <Provider store={ Store }>
    <Main />
  </Provider>
);